#include <hash_map>
#include <iostream>
#include <list>
#include <shared_mutex>

struct i_db {
  virtual bool begin_transaction() = 0;
  virtual bool commit_transaction() = 0;
  virtual bool abort_transaction() = 0;
  virtual std::string get(const std::string &key) = 0;
  virtual std::string set(const std::string &key, const std::string &data) = 0;
  virtual std::string deleteByKey(const std::string &key) = 0;
};

using hash_map = std::unordered_map<std::string, std::string>;

struct i_change {
  virtual void release(hash_map map);
  virtual ~i_change() = default;
};

class delete_change : public i_change {
  std::string key;

public:
  delete_change(std::string key) : key(std::move(key)) {}

  // i_change interface
public:
  void release(hash_map map) override { map.erase(key); }
};

class insert_change : public i_change {
  std::string key;
  std::string value;

public:
  insert_change(std::string key, std::string value)
      : key(std::move(key)), value(std::move(value)) {}

  // i_change interface
public:
  void release(hash_map map) override { map.insert(key, value); }
};

class db : public i_db {
  std::shared_mutex mutex;
  std::list<i_change *> transaction;
  bool transaction_beginned = false;
  hash_map map;

  // i_db interface
public:
  bool begin_transaction() override {
    if (transaction_beginned) {
      throw "You have already a beggined transaction";
    }
    transaction_beginned = true;
  }
  bool commit_transaction() override {
    if (!transaction_beginned) {
      throw "Begin a transaction first!";
    }

    std::unique_lock lock(mutex);
    bool successful = true;

    try {
      for (auto *change : transaction) {
        change->release(map);
        delete change;
      }
    } catch (...) {
      successful = false;
    }

    transaction.clear();
    transaction_beginned = false;

    return successful;
  }
  bool abort_transaction() override {
    if (!transaction_beginned) {
      return false;
    }

    transaction.clear();
    transaction_beginned = false;
    return true;
  }
  std::string get(const std::string &key) override {
    std::shared_lock lock(mutex);

    return map[key];
  }
  std::string set(const std::string &key, const std::string &data) override {
    if (!transaction_beginned)
      throw "Begin a transation first!";

    std::unique_lock lock(mutex);

    transaction.push_back(new insert_change{key, data});
    return {};
  }
  std::string deleteByKey(const std::string &key) override {
    if (!transaction_beginned)
      throw "Begin a transation first!";

    std::unique_lock lock(mutex);

    transaction.push_back(new delete_change{key});
    return {};
  }
};
